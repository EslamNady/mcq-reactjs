import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import 'antd/dist/antd.css';
import 'aos/dist/aos.css';
// import sass
import '../sass/app.sass'
import { Provider, connect } from 'react-redux'
import store from './redux/store'
import { LoadingOutlined } from '@ant-design/icons'

import Aos from 'aos'
//views
const EnrollPage = React.lazy(() => import('./views/enroll_name'))
const ExamPage = React.lazy(() => import('./views/exam'))

function App() {
  const loading = () => <div className="animated fadeIn pt-3 text-center font-Xlarge weight-500 text-theme-color pt-5"><LoadingOutlined /></div>;
  useEffect(() => {
    Aos.init({
      once: true
    })
  }, []);

  return (

    <React.Suspense fallback={loading()}>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path={`/enroll`} render={(props) => <EnrollPage {...props} />} />
            <Route path={`/exam`} render={(props) => <ExamPage {...props} />} />

            {/* default */}
            <Route render={(props) => <Redirect to={`/enroll`} />} />
          </Switch>
        </Router>
      </Provider>

    </React.Suspense>
  );
}

export default App;
