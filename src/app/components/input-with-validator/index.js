import React, { Component } from 'react';
import { validator, fieldValid, resetValidator, hideMessageFor } from '../../helperComponents/input-validator'
class ValidatorInput extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentDidMount() {
        const { name } = this.props
        hideMessageFor(this, name)
    }
    validate = () => {
        const { name, blur } = this.props
        if (blur) {
            fieldValid(this, name)
        }
    }
    render() {
        const { children, name, value, rules } = this.props
        return (
            <React.Fragment>
                <div onBlur={this.validate}>
                    {children}

                    <div className='input-error-msg-wrapper'>
                        {validator.message(name, value, rules)}
                        <span className={`server-error ${name}`}></span>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default ValidatorInput;