import SimpleReactValidator from 'simple-react-validator';
// import { compare_dates, compare_dates_hours } from '../helperFunctions/time-functions'

const errorMessages = {
    en: {
        email: 'Enter a valid email',
        phone: 'Enter phone number here',
        // min: 'Enter at least 8 characteristics',
        required: 'This field is required',
        numeric: 'This field should contains only numbers',
        // size: 'Enter 10 characters',
        alpha_space: 'Enter English letters only.',
        alpha_num_space: 'Enter English letters and numbers only.',
        alpha_num_dash_space: 'Enter English letters, numbers and dashes only.',
        alpha_num: 'Enter English letters and numbers only.',
        bp: 'Must be valid blood pressure',
        litters_spaces_only: 'Enter letters and spaces only.',
        saudi_phone: 'Enter Saudi phone number.',
        not_num: 'Numbers are not allowed.',
        today_or_before_today: 'Must be today or before today date.',
    },
    ar: {
        email: 'أدخل بريدًا إلكترونيًا هنا',
        phone: 'أدخل رقم الهاتف هنا',
        // min: 'أدخل ما لا يقل عن 8 الخصائص',
        required: 'هذا الحقل مطلوب ',
        numeric: 'يجب أن يحتوي هذا الحقل على أرقام فقط',
        // size: 'أدخل 10 أحرف Enter 10 characters',
        alpha_space: 'أدخل الحروف والمسافات فقط',
        bp: 'يجب أن يكون ضغط الدم',
        litters_spaces_only: 'أدخل الحروف والمسافات فقط ',
        saudi_phone: 'أدخل رقم الهاتف السعودي',
        not_num: 'الأرقام غير مسموح بها ',
        today_or_before_today: 'يجب أن يكون اليوم أو قبل تاريخ اليوم',
        today_or_after_today: 'يجب أن يكون اليوم أو بعد تاريخ اليوم',
    }
}
export const resetValidator = (that) => {
    validator.hideMessages()
    validator.purgeFields()
    that.forceUpdate();
}
export const validate = (that) => {
    if (!validator.allValid()) {
        validator.showMessages();
        that.forceUpdate();
        return false
    } else {
        validator.hideMessages()
        return true
    }
}
export const fieldValid = (that, name) => {
    if (!validator.fieldValid(name)) {
        validator.showMessageFor(name);
        that.forceUpdate();
        return false
    } else {
        validator.hideMessageFor(name)
        return true
    }
}
export const hideMessageFor = (that, name) => {
    validator.hideMessageFor(name)
    validator.purgeFields()
    that.forceUpdate();
}

const pathName = window.location.pathname
const locale = 'en'
//  pathName.includes('/ar') ? 'ar' : 'en'
console.log(pathName, locale)
export const validator = new SimpleReactValidator(
    {
        locale,
        messages: errorMessages[locale],
        validators: {
            alpha_num_dash_space: {  // name the rule 
                message: 'Enter English letters, numbers and dashes only.',
                rule: (val, params, validator) => {
                    const regex = /^[~`!@#$%^&*()-_+=[\]\{}|;':",.\/<>?a-zA-Z0-9 _]+$/;
                    return regex.test(val)
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                // required: true,  // optional
                // phone: true
            },
            alpha_num_space: {  // name the rule 
                message: 'Enter English letters, numbers only.',
                rule: (val, params, validator) => {
                    const regex = /^[~'\/<>?a-zA-Z0-9 _]+$/;
                    return regex.test(val)
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                // required: true,  // optional
                // phone: true
            },
            alpha_space: {  // name the rule 
                message: 'Enter English letters',
                rule: (val, params, validator) => {
                    const regex = /^[~'\/<>?a-zA-Z _]+$/;
                    return regex.test(val)
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                // required: true,  // optional
                // phone: true
            },
            not_only_numbers: {  // name the rule 
                message: 'Not only numbers',
                rule: (val, params, validator) => {
                    const regex = /(?!^\d+$)^.+$/;
                    return regex.test(val)
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                // required: true,  // optional
                // phone: true
            },



        },

    },

)