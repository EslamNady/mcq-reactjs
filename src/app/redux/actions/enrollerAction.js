import { SET_ENROLLER } from './types'
export const setEnrollerName = (name) => (dispatch) => {
    dispatch({
        type: SET_ENROLLER,
        payload: name
    })

}