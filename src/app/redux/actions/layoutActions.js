import { WINDOW_RESIZE } from './types'
export const resizeWindow = (window_size) => (dispatch) => {
    dispatch({
        type: WINDOW_RESIZE,
        payload: window_size,
    })

}