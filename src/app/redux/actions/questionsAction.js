import { RANDOMIZE_QUESTIONS_ORDER, GET_NEXT_QUESTION, GET_ANSWER, CALCULATE_SCORE, RESET_DATA } from './types'

export const resetData = () => (dispatch) => {
    dispatch({
        type: RESET_DATA,

    })

}
export const suffleQuestions = () => (dispatch) => {
    dispatch({
        type: RANDOMIZE_QUESTIONS_ORDER,

    })

}
export const getNextQuestion = () => (dispatch) => {
    dispatch({
        type: GET_NEXT_QUESTION
    })

}
export const getAnswerAction = (index, answer) => (dispatch) => {

    dispatch({
        type: GET_ANSWER,
        payload: {
            index,
            answer
        }
    })


}
export const calculateScore = () => (dispatch) => {

    dispatch({
        type: CALCULATE_SCORE
    })


}