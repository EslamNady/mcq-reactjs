import { SET_ENROLLER } from '../actions/types'
const initial_state = {
    name: ''
}

export default function (state = initial_state, action) {
    switch (action.type) {
        case SET_ENROLLER:
            return {
                ...state,
                name: action.payload
            }
        default:
            return state
    }
}