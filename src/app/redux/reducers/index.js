import { combineReducers } from 'redux'
import layoutReducer from './layoutReducer'
import enrollerReducer from './enrollerReducer'
import questionsReducer from './questionsReducer'

export default combineReducers({
    layout: layoutReducer,
    enroller: enrollerReducer,
    questions: questionsReducer,
})