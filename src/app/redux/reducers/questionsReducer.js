import { RANDOMIZE_QUESTIONS_ORDER, GET_NEXT_QUESTION, GET_ANSWER, CALCULATE_SCORE, RESET_DATA } from '../actions/types'
import { questions } from '../../utils/questions'
const initial_state = {
    exam_questions: [],
    current_question: 0,
    answers: [],
    score: 0,
}
function shuffle(array) {
    return JSON.parse(JSON.stringify(array)).sort(() => Math.random() - 0.5);
}
export default function (state = initial_state, action) {
    switch (action.type) {
        case RESET_DATA:
            return {
                ...state,
                exam_questions: [],
                current_question: 0,
                answers: [],
                score: 0,
            }
        case RANDOMIZE_QUESTIONS_ORDER:
            let exam_questions = shuffle(questions)
            exam_questions = exam_questions.map((question) => {
                question.answers = shuffle(question.answers)
                return question
            })
            return {
                ...state,
                exam_questions
            }
        case GET_NEXT_QUESTION:
            return {
                ...state,
                current_question: state.current_question < (state.exam_questions.length - 1) ?
                    state.current_question + 1 :
                    state.current_question
            }
        case GET_ANSWER:
            let answers = state.answers
            answers[action.payload.index] = action.payload.answer
            return {
                ...state,
                answers,
            }
        case CALCULATE_SCORE:
            let score = 0
            state.answers.forEach((answer) => {
                if (answer.correct) {
                    score++
                }
            })
            return {
                ...state,
                score
            }
        default:
            return state
    }
}