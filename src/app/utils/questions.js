export const questions = [{
    "id": 1,
    "content": "What is the longest that an elephant has ever lived?",
    "answers": [{
        "id": 1,
        "content": "17 years",
        "correct": false
    },
    {
        "id": 2,
        "content": "49 years",
        "correct": false
    },
    {
        "id": 3,
        "content": "86 years",
        "correct": true
    },
    {
        "id": 4,
        "content": "142 years",
        "correct": false
    }
    ]
},
{
    "id": 2,
    "content": "How many rings are on the Olympic flag?",
    "answers": [{
        "id": 1,
        "content": "None",
        "correct": false
    },
    {
        "id": 2,
        "content": "4",
        "correct": false
    },
    {
        "id": 3,
        "content": "5",
        "correct": true
    },
    {
        "id": 4,
        "content": "7",
        "correct": false
    }
    ]
},
{
    "id": 3,
    "content": "How did Spider-Man get his powers?",
    "answers": [{
        "id": 1,
        "content": "Military experiment gone awry",
        "correct": false
    },
    {
        "id": 2,
        "content": "Born with them",
        "correct": false
    },
    {
        "id": 3,
        "content": "Woke up with them after a strange dream",
        "correct": false
    },
    {
        "id": 4,
        "content": "Bitten by a radioactive spider",
        "correct": true
    }
    ]
},
{
    "id": 4,
    "content": "In darts, what's the most points you can score with a single throw?",
    "answers": [{
        "id": 1,
        "content": "20",
        "correct": false
    },
    {
        "id": 2,
        "content": "50",
        "correct": false
    },
    {
        "id": 3,
        "content": "60",
        "correct": true
    },
    {
        "id": 4,
        "content": "100",
        "correct": false
    }
    ]
},
{
    "id": 5,
    "content": "What are the main colors on the flag of Spain?",
    "answers": [{
        "id": 1,
        "content": "Black and yellow ",
        "correct": false
    },
    {
        "id": 2,
        "content": "Green and white",
        "correct": false
    },
    {
        "id": 3,
        "content": "Blue and white",
        "correct": false
    },
    {
        "id": 4,
        "content": "Red and yellow",
        "correct": true
    }
    ]
}
]
