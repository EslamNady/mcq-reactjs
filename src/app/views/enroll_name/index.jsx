import React, { useState, useEffect } from 'react';
import { Input } from 'antd'
import ValidatorInput from '../../components/input-with-validator'
import { validate, resetValidator } from '../../helperComponents/input-validator'
import { connect } from 'react-redux'
import { setEnrollerName } from '../../redux/actions/enrollerAction'
import { resetData } from '../../redux/actions/questionsAction'
function EnrollForm(props) {
    const [value, setValue] = useState(0);
    const forceUpdate = () => {
        setValue(value + 1);
    }
    const [name, setName] = useState('')
    useEffect(() => {
        document.title = `Enroll`;
        props.setEnrollerName('')
        props.resetData()
        resetValidator({ forceUpdate })
    }, []);
    const submitEnrollerName = (e) => {

        e.preventDefault()
        if (validate({ forceUpdate })) {

            props.setEnrollerName(name)
            props.history.push('/exam')
        }
    }
    return (
        <React.Fragment>
            <section className='animated fadeIn enroll-page py-5 d-flex align-items-center' style={{ minHeight: '100vh' }}>
                <div className='row mx-0 w-100'>
                    <div className='col-md-5 col-12 mx-auto'>
                        <div className='section-header'>
                            <h3 className='text-center text-theme-color weight-600 font-large '>
                                Enter your name to enroll
                            </h3>
                        </div>
                        <div className='form-wrapper'>
                            <form className='form' onSubmit={submitEnrollerName}>
                                <div className='form-input-wrapper'>
                                    <ValidatorInput blur={true} value={name} name='name' rules='required|alpha_space|not_only_numbers'>
                                        <div className='input-wrapper input-underline'>
                                            <Input value={name} name='name' onChange={(e) => { setName(e.target.value) }} placeholder='Name' />
                                        </div>
                                    </ValidatorInput>
                                </div>
                                <div className='btn-wrapper mt-4 text-center'>
                                    <button className='btn btn-theme-color font-Lsmall weight-600'>
                                        Enroll
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => ({

})
export default connect(mapStateToProps, { setEnrollerName, resetData })(EnrollForm);