import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { suffleQuestions } from '../../redux/actions/questionsAction'
import { Route, Switch, Redirect } from 'react-router-dom';
import QuestionsWrapper from './questions-wrapper'
import Score from './score'
import { Link } from 'react-router-dom'
import { HomeOutlined } from '@ant-design/icons'
function ExamPage(props) {
    useEffect(() => {
        if (!props.name) {
            props.history.push('/enroll')
        } else {
            document.title = `${props.name} Exam`;
            props.suffleQuestions()
        }


    }, []);
    return (
        <React.Fragment>
            <section className='animated fadeIn'>
                <div className='row mx-0 py-4'>
                    <div className='col-md-9 col-12 mx-auto'>
                        <div className='d-flex justify-content-between align-items-center'>
                            <div className='section-header'>
                                <h3 className='font-medium text-theme-color weight-600 m-0'>
                                    {props.name} Exam
                            </h3>
                            </div>
                            <div className=''>
                                <Link to={`/`} className='link-theme-color weight-600 font-Lsmall d-inline-flex align-items-center'><HomeOutlined />  <span className='px-2'>Home</span></Link>
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
            </section>
            <Switch>
                <Route path={`/exam/questions`} render={(props) => <QuestionsWrapper {...props} />} />
                <Route path={`/exam/score`} render={(props) => <Score {...props} />} />
                <Route render={(props) => <Redirect to={`/exam/questions`} />} />
            </Switch>
        </React.Fragment>
    );

}


const mapStateToProps = (state) => ({
    name: state.enroller.name,
    questions: state.questions.exam_questions
})
export default connect(mapStateToProps, { suffleQuestions })(ExamPage);