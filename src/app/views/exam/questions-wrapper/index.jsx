import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux'
import { getNextQuestion } from '../../../redux/actions/questionsAction'
import Question from './question'
import { validate, resetValidator } from '../../../helperComponents/input-validator'
function QuestionsWrapper(props) {
    const [value, setValue] = useState(0);
    const forceUpdate = () => {
        setValue(value + 1);
    }

    const next = () => {

        if (validate({ forceUpdate })) {

            props.getNextQuestion()
        }
    }
    const submit = () => {
        if (validate({ forceUpdate })) {

            props.history.push(`/exam/score`)
        }
    }
    return (
        <React.Fragment>
            <div className='animated fadeIn row mx-0'>
                <div className='col-md-9 col-12 mx-auto'>

                    <section className='questions-wrapper' style={{ minHeight: 'calc(100vh - 400px)', overflow: 'hidden' }}>
                        {
                            props.questions.length > 0 &&
                            <Question key={props.current} />
                        }

                    </section>
                    <section className='btn-wrapper'>
                        <div className='d-flex justify-content-end'>
                            {
                                props.current <= props.questions.length - 2 ?
                                    <button className='btn btn-theme-color font-Lsmall weight-600' onClick={next}>
                                        Next
                                   </button>
                                    :
                                    <button className='btn btn-theme-color font-Lsmall weight-600' onClick={submit}>
                                        Submit
                                    </button>
                            }

                        </div>
                    </section>
                </div>
            </div>
        </React.Fragment>
    );

}

const mapStateToProps = (state) => ({
    name: state.enroller.name,
    questions: state.questions.exam_questions,
    current: state.questions.current_question
})
export default connect(mapStateToProps, { getNextQuestion })(QuestionsWrapper);