import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux'
import { Radio } from 'antd'
import ValidatorInput from '../../../components/input-with-validator'
import { resetValidator } from '../../../helperComponents/input-validator'
import { getAnswerAction } from '../../../redux/actions/questionsAction'
function Question(props) {
    const { questions, current, answers } = props
    const [value, setValue] = useState(0);
    const forceUpdate = () => {
        setValue(value + 1);
    }
    useEffect(() => {
        resetValidator({ forceUpdate })
    }, []);
    const chooseAnswer = (e) => {
        props.getAnswerAction(current, e.target.value)
        forceUpdate()
    }
    return (
        <React.Fragment>
            <div className='animated fadeIn mb-3'>
                <div className='question-content' data-aos='zoom-out-left'>
                    <h4 className='font-large weight-600 text-gray-900 '>
                        {current + 1})  {questions[current].content}
                    </h4>
                </div>
                <hr />
                <div className='choices-wrapper' data-aos='zoom-out-left'>
                    <div className='form-wrapper'>
                        <div className='form'>
                            <div className='form-input-wrapper'>
                                <ValidatorInput value={answers[current]} name={`question-${current}`} rules='required'>
                                    <div className='input-wrapper'>
                                        <Radio.Group value={answers[current]} onChange={chooseAnswer}  >
                                            {
                                                questions[current].answers.map((choice, index) => (
                                                    <Radio.Button className='font-Lsmall weight-600' value={choice} key={index}>{index + 1}) {choice.content}</Radio.Button>
                                                ))
                                            }
                                        </Radio.Group>

                                    </div>
                                </ValidatorInput>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );

}

const mapStateToProps = (state) => ({

    questions: state.questions.exam_questions,
    current: state.questions.current_question,
    answers: state.questions.answers
})
export default connect(mapStateToProps, { getAnswerAction })(Question);