import React, { useState, useEffect } from 'react';
import { calculateScore } from '../../../redux/actions/questionsAction'
import { connect } from 'react-redux'

function ScorePage(props) {
    const { score } = props
    const [value, setValue] = useState(0);
    const forceUpdate = () => {
        setValue(value + 1);
    }
    useEffect(() => {
        props.calculateScore()
        forceUpdate()
    }, []);

    return (
        <React.Fragment>

            <div className='animated fadeIn row mx-0'>
                <div className='col-md-9 col-12 mx-auto'>



                    <div className='text-center mb-4'>
                        <h3 className='font-Xlarge weight-600 text-theme-color'>
                            Score
                    </h3>
                        <hr />
                        <div className='mt-4 font-large weight-600'>
                            {
                                score > 2 ?
                                    <span className='text-success'>Successed</span>
                                    :
                                    <span className='text-danger'>Failed</span>

                            }
                        </div>
                        <div className='font-medium weight-600 text-theme-color'>
                            <span className={`${score > 2 ? 'text-success' : 'text-danger'}`}>{score}</span>
                            <span> / 5</span>
                        </div>


                    </div>
                </div>
            </div>

        </React.Fragment>
    );

}

const mapStateToProps = (state) => ({
    score: state.questions.score
})
export default connect(mapStateToProps, { calculateScore })(ScorePage);